package com.pinyougou.sellergoods.service.impl;
import java.util.List;
import java.util.Map;

import com.pinyougou.sellergoods.service.SpecificationService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.pinyougou.mapper.TbSpecificationMapper;
import com.pinyougou.mapper.TbSpecificationOptionMapper;
import com.pinyougou.pojo.TbSpecification;
import com.pinyougou.pojo.TbSpecificationExample;
import com.pinyougou.pojo.TbSpecificationExample.Criteria;
import com.pinyougou.pojo.TbSpecificationOption;
import com.pinyougou.pojo.TbSpecificationOptionExample;
import com.pinyougou.pojogroup.Specification;

import entity.PageResult;

/**
 * 服务实现层
 * @author wei.ye
 *
 */
@Service //apache的服务 记得改回来
public class SpecificationServiceImpl implements SpecificationService {

	@Autowired
	private TbSpecificationMapper specificationMapper;
	
	@Autowired
	private TbSpecificationOptionMapper specificationOptionMapper;
	
	/**
	 * 查询全部
	 */
	@Override
	public List<TbSpecification> findAll() {
		return specificationMapper.selectByExample(null);
	}

	/**
	 * 按分页查询
	 */
	@Override
	public PageResult findPage(int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);		
		Page<TbSpecification> page=   (Page<TbSpecification>) specificationMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}

	/**
	 * 增加 规格名称和相应的规格选项数据
	 */
	/**
	 * 增加
	 */
	@Override
	public void add(Specification specification) {
		//获取规格实体
		TbSpecification tbspecification = specification.getSpecification();
		specificationMapper.insert(tbspecification);

		//获取规格选项集合
		List<TbSpecificationOption> specificationOptionList = specification.getSpecificationOptionList();
		for( TbSpecificationOption option:specificationOptionList){
			option.setSpecId(tbspecification.getId());//设置规格ID
			specificationOptionMapper.insert(option);//新增规格
		}
	}

	
	/**
	 * 修改 先把旧的规格删除再添加
	 */
	@Override
	public void update(Specification specification){
		//根据获取到的tb_specification来进行操作
		TbSpecification tb = specification.getSpecification();
		specificationMapper.updateByPrimaryKey(tb);

	     //需要把之前的规格选项删除再插入，有可能涉及到规格选项的增加和删除
        TbSpecificationOptionExample optionExample=new TbSpecificationOptionExample();
		TbSpecificationOptionExample.Criteria criteria = optionExample.createCriteria();
		 criteria.andSpecIdEqualTo(tb.getId());
		 //做batch delete
		specificationOptionMapper.deleteByExample(optionExample);

		//把新的规格插入
		for (TbSpecificationOption option :
				specification.getSpecificationOptionList()) {
			option.setSpecId(specification.getSpecification().getId()); //设置关联的外键 specifiation.id
			specificationOptionMapper.insert(option);

		}
		//插入完成
	}	
	
	/**
	 * 根据ID获取实体
	 * @param id
	 * @return
	 */
	@Override
	public Specification findOne(Long id){
		//规格名称
		Specification specification=new Specification();
		TbSpecification tbSpecification = specificationMapper.selectByPrimaryKey(id);
		specification.setSpecification(tbSpecification);
		//根据id去查找到相应的规格选项数据
		TbSpecificationOptionExample optionExample=new TbSpecificationOptionExample();
		TbSpecificationOptionExample.Criteria exampleCriteria = optionExample.createCriteria();
		exampleCriteria.andSpecIdEqualTo(id);//用specification 的id作为条件来查询对应的option表里的数据
		List<TbSpecificationOption> tbSpecificationOptions = specificationOptionMapper.selectByExample(optionExample);
		//设置查询出来的option表的数据
		specification.setSpecificationOptionList(tbSpecificationOptions);
		return specification;//组合实体类
	}

	/**
	 * 批量删除
	 */
	@Override
	public void delete(Long[] ids) {
		for(Long id:ids){
			//删除规格表数据
			specificationMapper.deleteByPrimaryKey(id);
			
			//删除规格选项表数据		
			TbSpecificationOptionExample example=new TbSpecificationOptionExample();
			TbSpecificationOptionExample.Criteria criteria = example.createCriteria();
			criteria.andSpecIdEqualTo(id);
			specificationOptionMapper.deleteByExample(example);
		}		
	}
	
	
		@Override
	public PageResult findPage(TbSpecification specification, int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		
		TbSpecificationExample example=new TbSpecificationExample();
		Criteria criteria = example.createCriteria();
		
		if(specification!=null){			
						if(specification.getSpecName()!=null && specification.getSpecName().length()>0){
				criteria.andSpecNameLike("%"+specification.getSpecName()+"%");
			}
	
		}
		
		Page<TbSpecification> page= (Page<TbSpecification>)specificationMapper.selectByExample(example);		
		return new PageResult(page.getTotal(), page.getResult());
	}

		@Override
		public List<Map> selectOptionList() {
			// TODO Auto-generated method stub
			//return specificationMapper.selectOptionList();
			return null;
		}
	
}
