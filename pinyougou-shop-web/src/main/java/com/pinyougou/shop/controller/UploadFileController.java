package com.pinyougou.shop.controller;

import com.pinyougou.shop.service.UploadFileImpl;
import entity.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Author: yewei
 * @Description:文件上传控制器
 * @Date:Create：in 2020/8/31 10:50
 * @Modified By：
 */
@RestController
@RequestMapping("/upload")
public class UploadFileController {
    @Autowired
    private UploadFileImpl uploadFileImpl;

    @Value("${FILE_SERVER_URL}")
    private String FILE_SERVER_URL;//文件服务器地址

    /**
     * 图片上传到服务器
     * @param file
     * @return
     */
    @RequestMapping(value = "/img",method = RequestMethod.POST)
    public Result imgUpload(MultipartFile file){
       Result result=null;
        try {
            Result execute = uploadFileImpl.execute(file, FILE_SERVER_URL);
            return  execute;
        }catch (Exception e){
         return   result=new Result(false,e.getMessage());
        }
    }
}
