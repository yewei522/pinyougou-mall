package com.pinyougou.shop.service;

import com.pinyougou.pojo.TbSeller;
import com.pinyougou.sellergoods.service.SellerService;
import org.apache.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: yewei
 * @Description: Spring-Security用户登录鉴权类 从数据库加载用户
 * @Date:Create：in 2020/8/26 22:25
 * @Modified By：
 */
public class UserDetailsServiceImpl implements UserDetailsService {
    private Logger logger=Logger.getLogger(UserDetailsServiceImpl.class);
   //setter注入
    private SellerService sellerService;
    public void setSellerService(SellerService sellerService) {
        this.sellerService = sellerService;
    }

    /**
     * @param username the username identifying the user whose data is required.
     * @return a fully populated user record (never <code>null</code>)
     * @throws UsernameNotFoundException if the user could not be found or the user has no
     *                                   GrantedAuthority
     *
     *                                通过按一定的规则从数据库加载用户的信息 比对后进行登录 若不符合要求则返回null 框架自动完成比对
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //获取用户名为 username的用户的权限的 集合
        List<GrantedAuthority> grantedAus=new ArrayList<GrantedAuthority>(3);
        grantedAus.add(new SimpleGrantedAuthority("ROLE_SELLER"));
        //去实现GrantedAuthority的子类--> SimpleGrantedAuthority :里面的role参数必须是spring-security配置的ROLE
        logger.info("❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤开始鉴权❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤");
        TbSeller user = sellerService.findOne(username);
        if(user!=null){
            if(user.getStatus().equals("1")){ //商家必须要被审核了之后才可以进行登录
                //SpringSecurity提供的User  需要传入认证器提供的角色权限的集合
                return  new User(username,user.getPassword(),grantedAus);
            }else {
                return  null;
            }
        }
        logger.info("❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤鉴权结束❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤");
        return null;
    }
}
