package com.pinyougou.shop.service;

import com.pinyougou.commonutils.FastDFSClient;
import entity.Result;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


/**
 * @Author: yewei
 * @Description:文件上传服务类
 * @Date:Create：in 2020/8/31 10:56
 * @Modified By：
 */
@Service
public class UploadFileImpl {
    /**
     * 文件上传的服务类
     * @param multipartFile
     * @param FILE_SERVER_URL:服务器URL：port
     * @return
     */
    public Result execute(MultipartFile multipartFile,String FILE_SERVER_URL) throws Exception {
        String oringName=multipartFile.getOriginalFilename();
        String extentds = oringName.substring(oringName.lastIndexOf("."));
        //通过FastDfs上传到服务器上去
        FastDFSClient fastDFSClient
                = new FastDFSClient("classpath:config/fdfs_client.conf");
        //3、执行上传处理
        String path = fastDFSClient.uploadFile(multipartFile.getBytes(), extentds);
        //4、拼接返回的 url 和 ip 地址，拼装成完整的 url
        String url = FILE_SERVER_URL + path;
        return new Result(true,url);
    }
}
