package com.pinyougou.Exception;

/**
 * @Author: yewei
 * @Description:
 * @Date:Create：in 2020/8/31 11:02
 * @Modified By：
 */
public class CommonException extends Exception{
    private  String  errcode;
    private  String  msg;

    /**
     * Constructs a new exception with {@code null} as its detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     */
    public CommonException(String errcode, String msg) {
        this.errcode = errcode;
        this.msg = msg;
    }
    public CommonException() {
    }

    public String getErrcode() {
        return errcode;
    }

    public void setErrcode(String errcode) {
        this.errcode = errcode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
