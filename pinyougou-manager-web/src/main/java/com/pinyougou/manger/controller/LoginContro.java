package com.pinyougou.manger.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: yewei
 * @Description:
 * @Date:Create：in 2020/8/25 10:35
 * @Modified By：
 */
@RestController
@RequestMapping("/login")
public class LoginContro {
    @RequestMapping(value = "/getName",method = RequestMethod.GET)
    public Map<String,Object> name(){
        Map<String,Object>  result=new HashMap<>();
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        result.put("name",name);
        return  result;
    }
}
