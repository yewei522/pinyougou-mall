package com.pinyougou.manger.controller;

import com.pinyougou.pojo.TbBrand;
import com.pinyougou.sellergoods.service.BrandService;
import entity.PageResult;
import entity.Result;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * ClassName: BrandController <br/>
 * Description: <br/>
 * date: 2020/3/27 18:49<br/>
 *
 * @author asus<br />
 * @since JDK 1.8
 */
@RestController
@RequestMapping("/brand")
public class BrandController {
    @Reference //调用dubbo的服务
    private BrandService brandService;
    @RequestMapping("/findAll")
    public List<TbBrand> findAll(){
        return brandService.findAll();
    }

    /**
     * 分页查询
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("/findByPage")
    public PageResult findPage(int page,int size){
        return brandService.findPage(page, size);
    }

    /**
     *添加品牌
     * @param brand
     * @return
     */
    @RequestMapping("/add")
    public Result add(@RequestBody TbBrand brand){
        try {
            brandService.add(brand);
            return  new Result(true,"添加成功");
        }catch (Exception e){
            return  new Result(false,"添加失敗");
        }
    }

    /**
     * 根据主键id去查询记录
     * @param id
     * @return
     */
    @RequestMapping("/findOne")
    public TbBrand findOne(@RequestParam(required = true,defaultValue ="0") Long id){
       return  brandService.findOne(id);
    }

    /**
     * 根据id进行品牌的修改
     * @param brand
     * @return
     */
    @RequestMapping("/updatebrand")
    public  Result updateById(@RequestBody TbBrand brand){
        try {
            brandService.update(brand);
            return  new Result(true,"修改成功");
        }catch (Exception e){
            return  new Result(false,"修改失敗");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("/batchDelete")
    public Result batcbhDelete(Long [] ids){
        try {
            brandService.delete(ids);
            return  new Result(true,"删除成功");
        }catch (Exception e){
            return  new Result(false,"删除失敗");
        }
    }
    @RequestMapping("/conditionSelect")
    public PageResult conditionSelect(@RequestBody TbBrand brand,int page,int size){
        PageResult result = brandService.findPage(brand, page, size);
        return result;
    }

    @RequestMapping("/selectOptionList")
    public List<Map> selectOptionList(){
        return brandService.selectOptionList();
    }

}
