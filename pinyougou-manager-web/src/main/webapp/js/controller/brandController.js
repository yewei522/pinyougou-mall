app.controller('brandController',function($scope,$http,$controller,brandService) {
    //页面重新加载或者刷新会触发该函数
    //刷新列表
    $controller('baseController',{$scope:$scope});//继承	 加继承$scope的范围扩大
    $scope.reloadList=function(){
        $scope.search( $scope.paginationConf.currentPage ,  $scope.paginationConf.itemsPerPage );
    }
    //分页
    $scope.findPage=function(page,size){
        brandService.findPage(page,size).success( //调用前端的服务层 来实现服务与业务的脱离
            function(response){
                $scope.list=response.rows;//显示当前页数据
                $scope.paginationConf.totalItems=response.total;//更新总记录数
            }
        );
    }
    /*保存和修改的方法*/
    $scope.save=function () {
        var prefix=null; //判断entity当前的状态
        if($scope.entity.id!=null){
            //有id说明是修改
            prefix=brandService.update($scope.entity);
        }else {
            prefix=brandService.save($scope.entity);
        }
        prefix.success(
            function(response){
                if(response.success){
                    $scope.reloadList();//刷新
                }else{
                    alert(response.msg);
                }
            }
        );
    }
    /*查询单条记录的方法*/
    $scope.findOne=function (id) { //传递id值
        brandService.findOne(id).success(function (response) {
            //给entity赋值 对象绑定值
            $scope.entity=response;
        });
    }
    /*批量删除*/
    $scope.batchdele=function () {
        //进行删除
        brandService.batchdele($scope.selectedids).success(function (response) {
            if(response.success){
                $scope.reloadList();//刷新下列表
            }else {
                window.alert(response.msg);
            }
        });
    }
    /*条件查询*/

    $scope.searchEntity={};
    //条件查询
    $scope.search=function(page,size){
        //调用服务层的方法
        brandService.search(page,size,$scope.searchEntity).success(
            function(response){
                $scope.list=response.rows;//显示当前页数据
                $scope.paginationConf.totalItems=response.total;//更新总记录数
            }
        );

    }


});