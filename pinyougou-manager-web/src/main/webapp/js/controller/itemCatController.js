 //控制层 
app.controller('itemCatController' ,function($scope,$controller   ,itemCatService){	
	
	$controller('baseController',{$scope:$scope});//继承
	
    //读取列表数据绑定到表单中  
	$scope.findAll=function(){
		itemCatService.findAll().success(
			function(response){
				$scope.list=response;
			}			
		);
	}    
	
	//分页
	$scope.findPage=function(page,rows){			
		itemCatService.findPage(page,rows).success(
			function(response){
				$scope.list=response.rows;	
				$scope.paginationConf.totalItems=response.total;//更新总记录数
			}			
		);
	}
	
	//查询实体 
	$scope.findOne=function(id){				
		itemCatService.findOne(id).success(
			function(response){
				$scope.entity= response;					
			}
		);				
	}
	$scope.parentId=0; //默认值为最顶层的 记录型变量
	//保存 
	$scope.save=function(){				
		var serviceObject;//服务层对象  				
		if($scope.entity.id!=null){//如果有ID  传入的有id则是修改的操作
			serviceObject=itemCatService.update( $scope.entity ); //修改  
		}else{
			//修改
			$scope.entity.parentId=$scope.parentId;//赋值纪录下来的parentId
			serviceObject=itemCatService.add( $scope.entity  );//增加 
		}				
		serviceObject.success(
			function(response){
				if(response.success){
					//重新查询 
		        	$scope.reloadList();//重新加载
				}else{
					alert(response.message);
				}
			}		
		);				
	}
	
	 
	//批量删除 
	$scope.dele=function(){			
		//获取选中的复选框			
		itemCatService.dele( $scope.selectIds ).success(
			function(response){
				if(response.success){
					$scope.reloadList();//刷新列表
					$scope.selectIds=[];
				}						
			}		
		);				
	}
	
	$scope.searchEntity={};//定义搜索对象 
	
	//搜索
	$scope.search=function(page,rows){			
		itemCatService.search(page,rows,$scope.searchEntity).success(
			function(response){
				$scope.list=response.rows;	
				$scope.paginationConf.totalItems=response.total;//更新总记录数
			}			
		);
	}
	//根据parentId去查找所有的节点的信息
    $scope.findSubCat=function (parentId) {
		$scope.parentId=parentId;//记住上级ID 每次查询后都回重新输一遍parentId的值
		itemCatService.findSubCat(parentId).success(
			function (response) {
				$scope.list=response; //绑定值
		});
	}
	/**********************面包屑的效果***********************************/
	$scope.grade=1;//默认为1级
	//设置级别
	$scope.setGrade=function(value){
		$scope.grade=value;
	}
	//读取列表
	$scope.selectList=function(p_entity){
		if($scope.grade==1){//如果为1级
			$scope.entity_1=null;
			$scope.entity_2=null;
		}
		if($scope.grade==2){//如果为2级
			$scope.entity_1=p_entity;
			$scope.entity_2=null;
		}
		if($scope.grade==3){//如果为3级
			$scope.entity_2=p_entity;
		}
		$scope.findSubCat(p_entity.id);	//查询此级下级列表
	}

	//新增或修改商品分类


});	
