/************************************************分层来写 service事务写法*********************************************/
app.service("brandService",function ($http) { //需要注入http下面有用到
    this.findPage=function (page,size) {
        return $http.get('/brand/findByPage.do?page='+page +'&size='+size);
    };
    /*保存的服务方法*/
    this.save=function (entity) {
        return $http.post('../brand/add.do',entity);
    };
    /*修改的服务方法*/
    this.update=function (entity) {
        return $http.post('../brand/updatebrand.do',entity);
    };
    /*批量删除*/
    this.batchdele=function (ids) {
        return $http.get('../brand/batchDelete.do?ids='+ids);
    };
    /*查找*/
    this.findOne=function (id) {
        return $http.get('../brand/findOne.do?id='+id);
    };
    /*条件查询*/
    this.search=function (page,size,searchEntity) {
        return $http.post('../brand/conditionSelect.do?page='+page +'&size='+size,searchEntity);
    };
});