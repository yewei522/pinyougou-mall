package entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: yewei
 * @Description:用于向前端生成JSON响应信息的工具类
 * @Date:Create：in 2020/4/9 23:24
 * @Modified By：
 */
public class Result implements Serializable {
   private boolean success;
   private String msg;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Result(boolean success, String msg) {
        this.success = success;
        this.msg = msg;
    }
}
